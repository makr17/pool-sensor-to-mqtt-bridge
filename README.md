# pool sensor to mqtt bridge

A simple shell script wrapping `rtl_433`, `jq` and `mosquitto_pub` to
bridge 433MHz sensor data from an rtl-sdr to mqtt for downstream
consumption.

My personal interest is in grabbing the temperature and humidity data
from two floating AmbientWeather pool sensors and incorporating them
as
[mqtt sensors](https://www.home-assistant.io/integrations/sensor.mqtt/)
in [Home Assistant](https://www.home-assistant.io/),
but it should be fairly simple to extend to other uses.

Currently takes two bits of configuration that must be provided in the
execution environment:

* MQHOST: the hostname or ip running the mqtt server
* MATCH: a string to match in the json output from `rtl_433`.  I use
  it to grab only the sensors I'm interested in, and filter out the
  neighbor's window security sensors.  To grab everything (no filter),
  a value of "." should do the trick.

given a sample like

```
{
  "time": "2021-02-16 23:57:22",
  "model": "Ambientweather-F007TH",
  "id": 24,
  "channel": 1,
  "battery_ok": 1,
  "temperature_F": 65.7,
  "humidity": 47,
  "mic": "CRC"
}
```

the above json paylod would be published to the mqtt topic

```
sensor/Ambientweather-F007TH/24
```

It can then be consumed in Home Assistant with a templated sensor like

```
sensor:
  - platform: mqtt
    name: ambient_spa_temp
    state_topic: sensor/Ambientweather-F007TH/24
    unique_id: ambientweather_F007TH_244_temp
    device_class: temperature
    unit_of_measurement: °F
    value_template: "{{ value_json.temperature_F }}"

  - platform: mqtt
    name: ambient_spa_humidity
    state_topic: sensor/Ambientweather-F007TH/24
    unique_id: ambientweather_F007TH_24_humidity
    device_class: humidity
    unit_of_measurement: "%"
    value_template: "{{ value_json.humidity }}"
```

build with

```
docker build -t <orgname>/poolsensor2mqtt .
```

and run, e.g. with `docker-compose`

```
version: '3.3'
services:
  poolsensor2mqtt:
    image: <orgname>/poolsensor2mqtt:latest
    restart: unless-stopped
    devices:
      - /dev/bus/usb
    environment:
      - MQHOST=<mqtt server>
      - MATCH=<filter string>
```

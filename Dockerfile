# use a slim debian container
FROM debian:bullseye-slim as scratch
# make sure we're running the latest
RUN apt update
# and security updates
RUN apt -y upgrade
# and install the bits we need
RUN apt -y install rtl-433 jq mosquitto-clients
# but clean up to keep things as slim as posible
RUN apt clean && rm -rf /var/lib/apt/lists/*
# copy in our entrypoint
COPY bin bin
CMD ["/bin/pooltemps2mqtt"]